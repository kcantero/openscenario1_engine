# OpenScenarioEngine

- This is a compilable draft of a partially generated OpenScenarioEngine
- Also refer to the [code generator](https://gitlab.eclipse.org/eclipse/simopenpass/osc1_engine_codegenerator).

## Background

The XML specification of the standard [ASAM **OpenSCENARIO 1.0**](https://www.asam.net/standards/detail/openscenario/) is generated from an UML model, also used to generate the [**OpenSCENARIO API** and **OpenSCENARIO 1.0 Parser**](https://github.com/RA-Consulting-GmbH/openscenario.api.test/), publicly hosted by [RA-Consulting GmbH](https://www.rac.de/).

When using the **OpenSCENARIO Parser** on a scenario file, it generates objects (implementing the **OpenSCENARIO API**). As such the (validated) information of the scenario file is available, but (a) not their logical relation and (b) the real business logic.

### Logical Relation

The idea of this engine is to use the same UML model and generator engine to generate a full set of concrete business logic objects that inherently know about their relationship.

For example, a `Condition` object holds either a `ByEntityCondition` or a `ByValueCondition`. Based on the current runtime-information it will then automatically create the according object.

### Business Logic Seam

During transformation, certain behaviors are injected, e.g. a `SimulationTimeCondition` needs access to an object implementing `mantle_api::IEnvironment`.
This behaviors extend the UML model and are described by a meta-model used in the [code generator](https://gitlab.eclipse.org/eclipse/simopenpass/osc1_engine_codegenerator).

The transformation generates a base class for each object, which is the seam to the concrete (hand made) implementation of the business logic

See for example the cascade for the `SimulationTimeCondition`:
- [src/generated/SimulationTimeConditionBase.h](src/generated/SimulationTimeConditionBase.h)
- [src/generated/SimulationTimeConditionBase.cpp](src/generated/SimulationTimeConditionBase.cpp)
- [src/implementation/SimulationTimeCondition.h](src/implementation/SimulationTimeCondition.h)
- [src/implementation/SimulationTimeCondition.cpp](src/implementation/SimulationTimeCondition.cpp)

## Need To Know

- This is *only* a working prototype.
  Right now, only `mantle_api::IEnvironment` is used and provided only as a mock (see [fakeEnvironment.h](fakeEnvironment.h).
  As several return values are simply ignored, many warnings are issued during compilation.
- For demonstration `SetDateTime()` and `GetDateTime()` are implemented to work in conjunction with the aformentioned `SimulationTimeCondition`.
- The paths to the [OpenSCENARIO API](https://github.com/RA-Consulting-GmbH/openscenario.api.test/), the [mantle_api](https://gitlab.eclipse.org/eclipse/simopenpass/scenario_api) and it's dependency (units)[https://github.com/nholthaus/units] need to set in [CMakeLists.txt](CMakeLists.txt)
- Tested under `Linux` using `gcc 10.2`
- Written in `C++17`

## Usage

```bash
mkdir build && cd build
cmake ..
cmake --build .
./openScenarioEngine ../example/scenario.xosc
```

## Issues

See [here](https://gitlab.eclipse.org/eclipse/simopenpass/openscenario1_engine/-/issues).

## License

[Eclipse Public License 2.0](LICENSE)
