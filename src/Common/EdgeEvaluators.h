/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "EdgeEvaluatorBase.h"

namespace OPENSCENARIO
{

class NoEdgeEvaluator : public EdgeEvaluatorBase
{
  public:
    void UpdateStatus(bool is_satisfied) noexcept override;
};

class RisingEdgeEvaluator : public EdgeEvaluatorBase
{
  public:
    void UpdateStatus(bool is_satisfied) noexcept override;

  private:
    bool last_is_satisfied_{false};
};

class FallingEdgeEvaluator : public EdgeEvaluatorBase
{
  public:
    void UpdateStatus(bool is_satisfied) noexcept override;

  private:
    bool last_is_satisfied_{false};
};

class RisingOrFallingEdgeEvaluator : public EdgeEvaluatorBase
{
  public:
    void UpdateStatus(bool is_satisfied) noexcept override;

  private:
    bool last_is_satisfied_{false};
};

} // namespace OPENSCENARIO
