/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/floating_point_helper.h>
#include <OpenScenarioParser/openScenarioLib/v1_0/generated/api/ApiClassInterfaces.h>

#include <type_traits>

namespace OPENSCENARIO
{
/// @brief Epsilon for equality of floating point types
static constexpr double RULE_FLOATING_POINT_EPSILON{1e-6};

/**
 * @brief Evaluator for Rule
 * @see NET_ASAM_OPENSCENARIO::v1_0::Rule::RuleEnum
 *
  * @note for equality of floating point types @ref RULE_FLOATING_POINT_EPSILON is used
 */
class Rule
{
  public:
    template <typename ConditionPtr>
    static auto from(ConditionPtr condition)
    {
        return Rule(condition->GetRule(), condition->GetValue());
    }

    static auto from(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITimeOfDayCondition> timeOfDayCondition)
    {
        std::cout << "Transformation of Rule for timeOfDayCondition not implemented yet. Returning 'EQUAL TO 0.0'.\n";
        // return Rule(condition->GetRule(), condition->GetDateTime());
        return Rule(NET_ASAM_OPENSCENARIO::v1_0::Rule::EQUAL_TO, 0.0);
    }

    /**
    * @brief Evaluates the stored rule, e.g. is current_value GREATHER_THAN(3)?
    *
    * @param current_value current value
    * @return true if current value meets rule, else false
    */
    bool IsSatisfied(double current_value) const
    {
        return compare_(current_value, value_);
    }

  private:
    //TODO: This is only a very basic implementation
    Rule(NET_ASAM_OPENSCENARIO::v1_0::Rule rule, std::string value)
    {
        try
        {
            value_ = std::stod(value);
        }
        catch (const std::exception& e)
        {
            throw std::invalid_argument(std::string(e.what()) + "\nUnable to parse rule with string value " + value + "\n");
        }
        SetCompare(rule);
    }

    Rule(NET_ASAM_OPENSCENARIO::v1_0::Rule rule, double value)
        : value_{value}
    {
        SetCompare(rule);
    }

  private:
    void SetCompare(NET_ASAM_OPENSCENARIO::v1_0::Rule rule)
    {
        if (rule == NET_ASAM_OPENSCENARIO::v1_0::Rule::GREATER_THAN)
        {
            compare_ = [](double lhs, double rhs) { return lhs > rhs; };
        }
        else if (rule == NET_ASAM_OPENSCENARIO::v1_0::Rule::LESS_THAN)
        {
            compare_ = [](double lhs, double rhs) { return lhs < rhs; };
        }
        else if (rule == NET_ASAM_OPENSCENARIO::v1_0::Rule::EQUAL_TO)
        {
            compare_ = [](double lhs, double rhs) {
                return mantle_api::IsEqual(lhs, rhs, RULE_FLOATING_POINT_EPSILON);
            };
        }
        else
        {
            throw std::runtime_error("Unknown rule");
        }
    }

    bool (*compare_)(double, double);  ///!< comparison method
    double value_;                     ///!< comparison value
};

}  // namespace OPENSCENARIO
