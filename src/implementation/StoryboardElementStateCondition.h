/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/StoryboardElementStateConditionBase.h"

namespace OPENSCENARIO
{
class StoryboardElementStateCondition : public StoryboardElementStateConditionBase
{
  public:
    explicit StoryboardElementStateCondition(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IStoryboardElementStateCondition> parsedStoryboardElementStateCondition,
        mantle_api::IEnvironment& environment)
        : StoryboardElementStateConditionBase(parsedStoryboardElementStateCondition, environment)
    {
    }

    bool IsSatisfied() const override;
};

}  // namespace OPENSCENARIO
