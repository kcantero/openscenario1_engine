/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "FollowTrajectoryAction.h"

namespace OPENSCENARIO
{

bool FollowTrajectoryAction::Complete() const
{
    std::cout << "FollowTrajectoryAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void FollowTrajectoryAction::Step()
{
    std::cout << "FollowTrajectoryAction::Step() not implemented yet. Doing nothing.\n";
}

void FollowTrajectoryAction::Stop()
{
    std::cout << "FollowTrajectoryAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO