/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/AbsoluteTargetLaneOffsetBase.h"

namespace OPENSCENARIO
{
class AbsoluteTargetLaneOffset : public AbsoluteTargetLaneOffsetBase
{
  public:
    explicit AbsoluteTargetLaneOffset(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAbsoluteTargetLaneOffset> parsedAbsoluteTargetLaneOffset)
        :  AbsoluteTargetLaneOffsetBase(parsedAbsoluteTargetLaneOffset)
    {
    }
};

}  // namespace OPENSCENARIO
