/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficAction.h"

namespace OPENSCENARIO
{

bool TrafficAction::Complete() const
{
    std::cout << "TrafficAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void TrafficAction::Step()
{
    std::cout << "TrafficAction::Step() not implemented yet. Doing nothing.\n";
}

void TrafficAction::Stop()
{
    std::cout << "TrafficAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO