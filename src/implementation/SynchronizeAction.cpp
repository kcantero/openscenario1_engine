/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SynchronizeAction.h"

namespace OPENSCENARIO
{

bool SynchronizeAction::Complete() const
{
    std::cout << "SynchronizeAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void SynchronizeAction::Step()
{
    std::cout << "SynchronizeAction::Step() not implemented yet. Doing nothing.\n";
}

void SynchronizeAction::Stop()
{
    std::cout << "SynchronizeAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO