/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSignalControllerAction.h"

namespace OPENSCENARIO
{

bool TrafficSignalControllerAction::Complete() const
{
    std::cout << "TrafficSignalControllerAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void TrafficSignalControllerAction::Step()
{
    std::cout << "TrafficSignalControllerAction::Step() not implemented yet. Doing nothing.\n";
}

void TrafficSignalControllerAction::Stop()
{
    std::cout << "TrafficSignalControllerAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO