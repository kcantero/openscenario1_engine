/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LateralDistanceAction.h"

namespace OPENSCENARIO
{

bool LateralDistanceAction::Complete() const
{
    std::cout << "LateralDistanceAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void LateralDistanceAction::Step()
{
    std::cout << "LateralDistanceAction::Step() not implemented yet. Doing nothing.\n";
}

void LateralDistanceAction::Stop()
{
    std::cout << "LateralDistanceAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO