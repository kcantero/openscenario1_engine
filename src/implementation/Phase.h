/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/PhaseBase.h"

namespace OPENSCENARIO
{
class Phase : public PhaseBase
{
  public:
    explicit Phase(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPhase> parsedPhase)
        :  PhaseBase(parsedPhase)
    {
    }

    ~Phase() override = default;
};

}  // namespace OPENSCENARIO
