/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OverrideClutchAction.h"

namespace OPENSCENARIO
{

bool OverrideClutchAction::Complete() const
{
    std::cout << "OverrideClutchAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void OverrideClutchAction::Step()
{
    std::cout << "OverrideClutchAction::Step() not implemented yet. Doing nothing.\n";
}

void OverrideClutchAction::Stop()
{
    std::cout << "OverrideClutchAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO