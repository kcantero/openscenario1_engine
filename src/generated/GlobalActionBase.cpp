/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "GlobalActionBase.h"

#include "EntityAction.h"
#include "EnvironmentAction.h"
#include "InfrastructureAction.h"
#include "OpenScenarioEngineFactory.h"
#include "ParameterAction.h"
#include "TrafficAction.h"

namespace OPENSCENARIO
{
static GlobalActionBase::GlobalAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IGlobalAction> globalAction)
{
    if (auto element = globalAction->GetEnvironmentAction(); element)
    {
        return Builder::transform<EnvironmentAction>(element);
    }
    if (auto element = globalAction->GetEntityAction(); element)
    {
        return Builder::transform<EntityAction>(element);
    }
    if (auto element = globalAction->GetParameterAction(); element)
    {
        return Builder::transform<ParameterAction>(element);
    }
    if (auto element = globalAction->GetInfrastructureAction(); element)
    {
        return Builder::transform<InfrastructureAction>(element);
    }
    if (auto element = globalAction->GetTrafficAction(); element)
    {
        return Builder::transform<TrafficAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IGlobalAction");
}

GlobalActionBase::GlobalActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IGlobalAction> globalAction)
    : Node{"GlobalActionBase"}, globalAction_{(std::cout << "GlobalActionBase instantiating globalAction_" << std::endl, resolve_choices(globalAction))}

{
}

bool GlobalActionBase::Complete() const
{
    std::cout << "GlobalActionBase complete?\n";
    return OPENSCENARIO::Complete(globalAction_);
}

void GlobalActionBase::Step()
{
    std::cout << "GlobalActionBase step!\n";
    OPENSCENARIO::Step(globalAction_);
}

void GlobalActionBase::Stop()
{
    std::cout << "GlobalActionBase stop!\n";
    OPENSCENARIO::Stop(globalAction_);
}

}  // namespace OPENSCENARIO
