/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <OpenScenarioParser/openScenarioLib/v1_0/generated/api/ApiClassInterfaces.h>

#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "Common/Node.h"

namespace OPENSCENARIO
{
class FileBase : public Node
{
  public:
    explicit FileBase(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IFile> file);
    FileBase(FileBase&&) = default;
    ~FileBase() override = default;

  protected:
};

}  // namespace OPENSCENARIO
