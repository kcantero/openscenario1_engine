/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EnvironmentBase.h"

#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"
#include "RoadCondition.h"
#include "TimeOfDay.h"
#include "Weather.h"

namespace OPENSCENARIO
{
EnvironmentBase::EnvironmentBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEnvironment> environment)
    : Node{"EnvironmentBase"}, timeOfDay_{(std::cout << "EnvironmentBase instantiating timeOfDay_" << std::endl, Builder::transform<TimeOfDay>(environment->GetTimeOfDay()))}, weather_{(std::cout << "EnvironmentBase instantiating weather_" << std::endl, Builder::transform<Weather>(environment->GetWeather()))}, roadCondition_{(std::cout << "EnvironmentBase instantiating roadCondition_" << std::endl, Builder::transform<RoadCondition>(environment->GetRoadCondition()))}, parameterDeclarations_{(std::cout << "EnvironmentBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(environment, &NET_ASAM_OPENSCENARIO::v1_0::IEnvironment::GetParameterDeclarations))}

{
}

bool EnvironmentBase::Complete() const
{
    std::cout << "EnvironmentBase complete?\n";
    return OPENSCENARIO::Complete(timeOfDay_) && OPENSCENARIO::Complete(weather_) && OPENSCENARIO::Complete(roadCondition_) && OPENSCENARIO::Complete(parameterDeclarations_);
}

void EnvironmentBase::Step()
{
    std::cout << "EnvironmentBase step!\n";
    OPENSCENARIO::Step(timeOfDay_);
    OPENSCENARIO::Step(weather_);
    OPENSCENARIO::Step(roadCondition_);
    OPENSCENARIO::Step(parameterDeclarations_);
}

void EnvironmentBase::Stop()
{
    std::cout << "EnvironmentBase stop!\n";
    OPENSCENARIO::Stop(timeOfDay_);
    OPENSCENARIO::Stop(weather_);
    OPENSCENARIO::Stop(roadCondition_);
    OPENSCENARIO::Stop(parameterDeclarations_);
}

}  // namespace OPENSCENARIO
