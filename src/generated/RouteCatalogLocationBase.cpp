/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RouteCatalogLocationBase.h"

#include "Directory.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
RouteCatalogLocationBase::RouteCatalogLocationBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRouteCatalogLocation> routeCatalogLocation)
    : Node{"RouteCatalogLocationBase"}, directory_{(std::cout << "RouteCatalogLocationBase instantiating directory_" << std::endl, Builder::transform<Directory>(routeCatalogLocation->GetDirectory()))}

{
}

bool RouteCatalogLocationBase::Complete() const
{
    std::cout << "RouteCatalogLocationBase complete?\n";
    return OPENSCENARIO::Complete(directory_);
}

void RouteCatalogLocationBase::Step()
{
    std::cout << "RouteCatalogLocationBase step!\n";
    OPENSCENARIO::Step(directory_);
}

void RouteCatalogLocationBase::Stop()
{
    std::cout << "RouteCatalogLocationBase stop!\n";
    OPENSCENARIO::Stop(directory_);
}

}  // namespace OPENSCENARIO
