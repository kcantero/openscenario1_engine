/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "VehicleCategoryDistributionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "VehicleCategoryDistributionEntry.h"

namespace OPENSCENARIO
{
VehicleCategoryDistributionBase::VehicleCategoryDistributionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IVehicleCategoryDistribution> vehicleCategoryDistribution)
    : Node{"VehicleCategoryDistributionBase"}, vehicleCategoryDistributionEntries_{(std::cout << "VehicleCategoryDistributionBase instantiating vehicleCategoryDistributionEntries_" << std::endl, Builder::transform<VehicleCategoryDistributionEntry, VehicleCategoryDistributionEntries_t>(vehicleCategoryDistribution, &NET_ASAM_OPENSCENARIO::v1_0::IVehicleCategoryDistribution::GetVehicleCategoryDistributionEntries))}

{
}

bool VehicleCategoryDistributionBase::Complete() const
{
    std::cout << "VehicleCategoryDistributionBase complete?\n";
    return OPENSCENARIO::Complete(vehicleCategoryDistributionEntries_);
}

void VehicleCategoryDistributionBase::Step()
{
    std::cout << "VehicleCategoryDistributionBase step!\n";
    OPENSCENARIO::Step(vehicleCategoryDistributionEntries_);
}

void VehicleCategoryDistributionBase::Stop()
{
    std::cout << "VehicleCategoryDistributionBase stop!\n";
    OPENSCENARIO::Stop(vehicleCategoryDistributionEntries_);
}

}  // namespace OPENSCENARIO
