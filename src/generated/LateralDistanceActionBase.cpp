/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LateralDistanceActionBase.h"

#include "DynamicConstraints.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
LateralDistanceActionBase::LateralDistanceActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILateralDistanceAction> lateralDistanceAction)
    : Node{"LateralDistanceActionBase"}, dynamicConstraints_{(std::cout << "LateralDistanceActionBase instantiating dynamicConstraints_" << std::endl, Builder::transform<DynamicConstraints>(lateralDistanceAction->GetDynamicConstraints()))}

{
}

bool LateralDistanceActionBase::Complete() const
{
    std::cout << "LateralDistanceActionBase complete?\n";
    return OPENSCENARIO::Complete(dynamicConstraints_);
}

void LateralDistanceActionBase::Step()
{
    std::cout << "LateralDistanceActionBase step!\n";
    OPENSCENARIO::Step(dynamicConstraints_);
}

void LateralDistanceActionBase::Stop()
{
    std::cout << "LateralDistanceActionBase stop!\n";
    OPENSCENARIO::Stop(dynamicConstraints_);
}

}  // namespace OPENSCENARIO
