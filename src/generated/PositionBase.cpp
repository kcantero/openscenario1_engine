/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PositionBase.h"

#include "LanePosition.h"
#include "OpenScenarioEngineFactory.h"
#include "RelativeLanePosition.h"
#include "RelativeObjectPosition.h"
#include "RelativeRoadPosition.h"
#include "RelativeWorldPosition.h"
#include "RoadPosition.h"
#include "RoutePosition.h"
#include "WorldPosition.h"

namespace OPENSCENARIO
{
static PositionBase::Position_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPosition> position)
{
    if (auto element = position->GetWorldPosition(); element)
    {
        return Builder::transform<WorldPosition>(element);
    }
    if (auto element = position->GetRelativeWorldPosition(); element)
    {
        return Builder::transform<RelativeWorldPosition>(element);
    }
    if (auto element = position->GetRelativeObjectPosition(); element)
    {
        return Builder::transform<RelativeObjectPosition>(element);
    }
    if (auto element = position->GetRoadPosition(); element)
    {
        return Builder::transform<RoadPosition>(element);
    }
    if (auto element = position->GetRelativeRoadPosition(); element)
    {
        return Builder::transform<RelativeRoadPosition>(element);
    }
    if (auto element = position->GetLanePosition(); element)
    {
        return Builder::transform<LanePosition>(element);
    }
    if (auto element = position->GetRelativeLanePosition(); element)
    {
        return Builder::transform<RelativeLanePosition>(element);
    }
    if (auto element = position->GetRoutePosition(); element)
    {
        return Builder::transform<RoutePosition>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IPosition");
}

PositionBase::PositionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPosition> position)
    : Node{"PositionBase"}, position_{(std::cout << "PositionBase instantiating position_" << std::endl, resolve_choices(position))}

{
}

bool PositionBase::Complete() const
{
    std::cout << "PositionBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void PositionBase::Step()
{
    std::cout << "PositionBase step!\n";
    OPENSCENARIO::Step(position_);
}

void PositionBase::Stop()
{
    std::cout << "PositionBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO
