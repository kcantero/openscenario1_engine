/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "BoundingBoxBase.h"

#include "Center.h"
#include "Dimensions.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
BoundingBoxBase::BoundingBoxBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IBoundingBox> boundingBox)
    : Node{"BoundingBoxBase"}, center_{(std::cout << "BoundingBoxBase instantiating center_" << std::endl, Builder::transform<Center>(boundingBox->GetCenter()))}, dimensions_{(std::cout << "BoundingBoxBase instantiating dimensions_" << std::endl, Builder::transform<Dimensions>(boundingBox->GetDimensions()))}

{
}

bool BoundingBoxBase::Complete() const
{
    std::cout << "BoundingBoxBase complete?\n";
    return OPENSCENARIO::Complete(center_) && OPENSCENARIO::Complete(dimensions_);
}

void BoundingBoxBase::Step()
{
    std::cout << "BoundingBoxBase step!\n";
    OPENSCENARIO::Step(center_);
    OPENSCENARIO::Step(dimensions_);
}

void BoundingBoxBase::Stop()
{
    std::cout << "BoundingBoxBase stop!\n";
    OPENSCENARIO::Stop(center_);
    OPENSCENARIO::Stop(dimensions_);
}

}  // namespace OPENSCENARIO
