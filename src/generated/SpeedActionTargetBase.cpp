/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SpeedActionTargetBase.h"

#include "AbsoluteTargetSpeed.h"
#include "OpenScenarioEngineFactory.h"
#include "RelativeTargetSpeed.h"

namespace OPENSCENARIO
{
static SpeedActionTargetBase::SpeedActionTarget_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ISpeedActionTarget> speedActionTarget)
{
    if (auto element = speedActionTarget->GetRelativeTargetSpeed(); element)
    {
        return Builder::transform<RelativeTargetSpeed>(element);
    }
    if (auto element = speedActionTarget->GetAbsoluteTargetSpeed(); element)
    {
        return Builder::transform<AbsoluteTargetSpeed>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ISpeedActionTarget");
}

SpeedActionTargetBase::SpeedActionTargetBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ISpeedActionTarget> speedActionTarget)
    : Node{"SpeedActionTargetBase"}, speedActionTarget_{(std::cout << "SpeedActionTargetBase instantiating speedActionTarget_" << std::endl, resolve_choices(speedActionTarget))}

{
}

bool SpeedActionTargetBase::Complete() const
{
    std::cout << "SpeedActionTargetBase complete?\n";
    return OPENSCENARIO::Complete(speedActionTarget_);
}

void SpeedActionTargetBase::Step()
{
    std::cout << "SpeedActionTargetBase step!\n";
    OPENSCENARIO::Step(speedActionTarget_);
}

void SpeedActionTargetBase::Stop()
{
    std::cout << "SpeedActionTargetBase stop!\n";
    OPENSCENARIO::Stop(speedActionTarget_);
}

}  // namespace OPENSCENARIO
