/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "VehicleCatalogLocationBase.h"

#include "Directory.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
VehicleCatalogLocationBase::VehicleCatalogLocationBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IVehicleCatalogLocation> vehicleCatalogLocation)
    : Node{"VehicleCatalogLocationBase"}, directory_{(std::cout << "VehicleCatalogLocationBase instantiating directory_" << std::endl, Builder::transform<Directory>(vehicleCatalogLocation->GetDirectory()))}

{
}

bool VehicleCatalogLocationBase::Complete() const
{
    std::cout << "VehicleCatalogLocationBase complete?\n";
    return OPENSCENARIO::Complete(directory_);
}

void VehicleCatalogLocationBase::Step()
{
    std::cout << "VehicleCatalogLocationBase step!\n";
    OPENSCENARIO::Step(directory_);
}

void VehicleCatalogLocationBase::Stop()
{
    std::cout << "VehicleCatalogLocationBase stop!\n";
    OPENSCENARIO::Stop(directory_);
}

}  // namespace OPENSCENARIO
