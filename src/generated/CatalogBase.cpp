/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "CatalogBase.h"

#include "Controller.h"
#include "Environment.h"
#include "Maneuver.h"
#include "MiscObject.h"
#include "OpenScenarioEngineFactory.h"
#include "Pedestrian.h"
#include "Route.h"
#include "Trajectory.h"
#include "Vehicle.h"

namespace OPENSCENARIO
{
CatalogBase::CatalogBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICatalog> catalog)
    : Node{"CatalogBase"}, vehicles_{(std::cout << "CatalogBase instantiating vehicles_" << std::endl, Builder::transform<Vehicle, Vehicles_t>(catalog, &NET_ASAM_OPENSCENARIO::v1_0::ICatalog::GetVehicles))}, controllers_{(std::cout << "CatalogBase instantiating controllers_" << std::endl, Builder::transform<Controller, Controllers_t>(catalog, &NET_ASAM_OPENSCENARIO::v1_0::ICatalog::GetControllers))}, pedestrians_{(std::cout << "CatalogBase instantiating pedestrians_" << std::endl, Builder::transform<Pedestrian, Pedestrians_t>(catalog, &NET_ASAM_OPENSCENARIO::v1_0::ICatalog::GetPedestrians))}, miscObjects_{(std::cout << "CatalogBase instantiating miscObjects_" << std::endl, Builder::transform<MiscObject, MiscObjects_t>(catalog, &NET_ASAM_OPENSCENARIO::v1_0::ICatalog::GetMiscObjects))}, environments_{(std::cout << "CatalogBase instantiating environments_" << std::endl, Builder::transform<Environment, Environments_t>(catalog, &NET_ASAM_OPENSCENARIO::v1_0::ICatalog::GetEnvironments))}, maneuvers_{(std::cout << "CatalogBase instantiating maneuvers_" << std::endl, Builder::transform<Maneuver, Maneuvers_t>(catalog, &NET_ASAM_OPENSCENARIO::v1_0::ICatalog::GetManeuvers))}, trajectories_{(std::cout << "CatalogBase instantiating trajectories_" << std::endl, Builder::transform<Trajectory, Trajectories_t>(catalog, &NET_ASAM_OPENSCENARIO::v1_0::ICatalog::GetTrajectories))}, routes_{(std::cout << "CatalogBase instantiating routes_" << std::endl, Builder::transform<Route, Routes_t>(catalog, &NET_ASAM_OPENSCENARIO::v1_0::ICatalog::GetRoutes))}

{
}

bool CatalogBase::Complete() const
{
    std::cout << "CatalogBase complete?\n";
    return OPENSCENARIO::Complete(vehicles_) && OPENSCENARIO::Complete(controllers_) && OPENSCENARIO::Complete(pedestrians_) && OPENSCENARIO::Complete(miscObjects_) && OPENSCENARIO::Complete(environments_) && OPENSCENARIO::Complete(maneuvers_) && OPENSCENARIO::Complete(trajectories_) && OPENSCENARIO::Complete(routes_);
}

void CatalogBase::Step()
{
    std::cout << "CatalogBase step!\n";
    OPENSCENARIO::Step(vehicles_);
    OPENSCENARIO::Step(controllers_);
    OPENSCENARIO::Step(pedestrians_);
    OPENSCENARIO::Step(miscObjects_);
    OPENSCENARIO::Step(environments_);
    OPENSCENARIO::Step(maneuvers_);
    OPENSCENARIO::Step(trajectories_);
    OPENSCENARIO::Step(routes_);
}

void CatalogBase::Stop()
{
    std::cout << "CatalogBase stop!\n";
    OPENSCENARIO::Stop(vehicles_);
    OPENSCENARIO::Stop(controllers_);
    OPENSCENARIO::Stop(pedestrians_);
    OPENSCENARIO::Stop(miscObjects_);
    OPENSCENARIO::Stop(environments_);
    OPENSCENARIO::Stop(maneuvers_);
    OPENSCENARIO::Stop(trajectories_);
    OPENSCENARIO::Stop(routes_);
}

}  // namespace OPENSCENARIO
