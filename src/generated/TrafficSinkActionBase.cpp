/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSinkActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"
#include "TrafficDefinition.h"

namespace OPENSCENARIO
{
TrafficSinkActionBase::TrafficSinkActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficSinkAction> trafficSinkAction)
    : Node{"TrafficSinkActionBase"}, position_{(std::cout << "TrafficSinkActionBase instantiating position_" << std::endl, Builder::transform<Position>(trafficSinkAction->GetPosition()))}, trafficDefinition_{(std::cout << "TrafficSinkActionBase instantiating trafficDefinition_" << std::endl, Builder::transform<TrafficDefinition>(trafficSinkAction->GetTrafficDefinition()))}

{
}

bool TrafficSinkActionBase::Complete() const
{
    std::cout << "TrafficSinkActionBase complete?\n";
    return OPENSCENARIO::Complete(position_) && OPENSCENARIO::Complete(trafficDefinition_);
}

void TrafficSinkActionBase::Step()
{
    std::cout << "TrafficSinkActionBase step!\n";
    OPENSCENARIO::Step(position_);
    OPENSCENARIO::Step(trafficDefinition_);
}

void TrafficSinkActionBase::Stop()
{
    std::cout << "TrafficSinkActionBase stop!\n";
    OPENSCENARIO::Stop(position_);
    OPENSCENARIO::Stop(trafficDefinition_);
}

}  // namespace OPENSCENARIO
