/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ActionBase.h"

#include "GlobalAction.h"
#include "OpenScenarioEngineFactory.h"
#include "PrivateAction.h"
#include "UserDefinedAction.h"

namespace OPENSCENARIO
{
static ActionBase::Action_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAction> action)
{
    if (auto element = action->GetGlobalAction(); element)
    {
        return Builder::transform<GlobalAction>(element);
    }
    if (auto element = action->GetUserDefinedAction(); element)
    {
        return Builder::transform<UserDefinedAction>(element);
    }
    if (auto element = action->GetPrivateAction(); element)
    {
        return Builder::transform<PrivateAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IAction");
}

ActionBase::ActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAction> action)
    : Node{"ActionBase"}, action_{(std::cout << "ActionBase instantiating action_" << std::endl, resolve_choices(action))}

{
}

bool ActionBase::Complete() const
{
    std::cout << "ActionBase complete?\n";
    return OPENSCENARIO::Complete(action_);
}

void ActionBase::Step()
{
    std::cout << "ActionBase step!\n";
    OPENSCENARIO::Step(action_);
}

void ActionBase::Stop()
{
    std::cout << "ActionBase stop!\n";
    OPENSCENARIO::Stop(action_);
}

}  // namespace OPENSCENARIO
