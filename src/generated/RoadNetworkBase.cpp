/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadNetworkBase.h"

#include "File.h"
#include "OpenScenarioEngineFactory.h"
#include "TrafficSignalController.h"

namespace OPENSCENARIO
{
RoadNetworkBase::RoadNetworkBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRoadNetwork> roadNetwork)
    : Node{"RoadNetworkBase"}, logicFile_{(std::cout << "RoadNetworkBase instantiating logicFile_" << std::endl, Builder::transform<File>(roadNetwork->GetLogicFile()))}, sceneGraphFile_{(std::cout << "RoadNetworkBase instantiating sceneGraphFile_" << std::endl, Builder::transform<File>(roadNetwork->GetSceneGraphFile()))}, trafficSignals_{(std::cout << "RoadNetworkBase instantiating trafficSignals_" << std::endl, Builder::transform<TrafficSignalController, TrafficSignalControllers_t>(roadNetwork, &NET_ASAM_OPENSCENARIO::v1_0::IRoadNetwork::GetTrafficSignals))}

{
}

bool RoadNetworkBase::Complete() const
{
    std::cout << "RoadNetworkBase complete?\n";
    return OPENSCENARIO::Complete(logicFile_) && OPENSCENARIO::Complete(sceneGraphFile_) && OPENSCENARIO::Complete(trafficSignals_);
}

void RoadNetworkBase::Step()
{
    std::cout << "RoadNetworkBase step!\n";
    OPENSCENARIO::Step(logicFile_);
    OPENSCENARIO::Step(sceneGraphFile_);
    OPENSCENARIO::Step(trafficSignals_);
}

void RoadNetworkBase::Stop()
{
    std::cout << "RoadNetworkBase stop!\n";
    OPENSCENARIO::Stop(logicFile_);
    OPENSCENARIO::Stop(sceneGraphFile_);
    OPENSCENARIO::Stop(trafficSignals_);
}

}  // namespace OPENSCENARIO
