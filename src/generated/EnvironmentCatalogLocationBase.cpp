/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EnvironmentCatalogLocationBase.h"

#include "Directory.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
EnvironmentCatalogLocationBase::EnvironmentCatalogLocationBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEnvironmentCatalogLocation> environmentCatalogLocation)
    : Node{"EnvironmentCatalogLocationBase"}, directory_{(std::cout << "EnvironmentCatalogLocationBase instantiating directory_" << std::endl, Builder::transform<Directory>(environmentCatalogLocation->GetDirectory()))}

{
}

bool EnvironmentCatalogLocationBase::Complete() const
{
    std::cout << "EnvironmentCatalogLocationBase complete?\n";
    return OPENSCENARIO::Complete(directory_);
}

void EnvironmentCatalogLocationBase::Step()
{
    std::cout << "EnvironmentCatalogLocationBase step!\n";
    OPENSCENARIO::Step(directory_);
}

void EnvironmentCatalogLocationBase::Stop()
{
    std::cout << "EnvironmentCatalogLocationBase stop!\n";
    OPENSCENARIO::Stop(directory_);
}

}  // namespace OPENSCENARIO
