/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSignalControllerActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Phase.h"

namespace OPENSCENARIO
{
TrafficSignalControllerActionBase::TrafficSignalControllerActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficSignalControllerAction> trafficSignalControllerAction)
    : Node{"TrafficSignalControllerActionBase"}, phaseRefs_{(std::cout << "TrafficSignalControllerActionBase instantiating phaseRefs_" << std::endl, Builder::transform<Phase, Phases_t>(trafficSignalControllerAction, &NET_ASAM_OPENSCENARIO::v1_0::ITrafficSignalControllerAction::GetPhaseRef))}

{
}

bool TrafficSignalControllerActionBase::Complete() const
{
    std::cout << "TrafficSignalControllerActionBase complete?\n";
    return OPENSCENARIO::Complete(phaseRefs_);
}

void TrafficSignalControllerActionBase::Step()
{
    std::cout << "TrafficSignalControllerActionBase step!\n";
    OPENSCENARIO::Step(phaseRefs_);
}

void TrafficSignalControllerActionBase::Stop()
{
    std::cout << "TrafficSignalControllerActionBase stop!\n";
    OPENSCENARIO::Stop(phaseRefs_);
}

}  // namespace OPENSCENARIO
