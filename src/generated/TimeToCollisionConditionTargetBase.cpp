/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TimeToCollisionConditionTargetBase.h"

#include "EntityRef.h"
#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
static TimeToCollisionConditionTargetBase::TimeToCollisionConditionTarget_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITimeToCollisionConditionTarget> timeToCollisionConditionTarget)
{
    if (auto element = timeToCollisionConditionTarget->GetPosition(); element)
    {
        return Builder::transform<Position>(element);
    }
    if (auto element = timeToCollisionConditionTarget->GetEntityRef(); element)
    {
        return Builder::transform<EntityRef>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ITimeToCollisionConditionTarget");
}

TimeToCollisionConditionTargetBase::TimeToCollisionConditionTargetBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITimeToCollisionConditionTarget> timeToCollisionConditionTarget)
    : Node{"TimeToCollisionConditionTargetBase"}, timeToCollisionConditionTarget_{(std::cout << "TimeToCollisionConditionTargetBase instantiating timeToCollisionConditionTarget_" << std::endl, resolve_choices(timeToCollisionConditionTarget))}

{
}

bool TimeToCollisionConditionTargetBase::Complete() const
{
    std::cout << "TimeToCollisionConditionTargetBase complete?\n";
    return OPENSCENARIO::Complete(timeToCollisionConditionTarget_);
}

void TimeToCollisionConditionTargetBase::Step()
{
    std::cout << "TimeToCollisionConditionTargetBase step!\n";
    OPENSCENARIO::Step(timeToCollisionConditionTarget_);
}

void TimeToCollisionConditionTargetBase::Stop()
{
    std::cout << "TimeToCollisionConditionTargetBase stop!\n";
    OPENSCENARIO::Stop(timeToCollisionConditionTarget_);
}

}  // namespace OPENSCENARIO
