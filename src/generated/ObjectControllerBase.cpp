/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ObjectControllerBase.h"

#include "CatalogReference.h"
#include "Controller.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
static ObjectControllerBase::ObjectController_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IObjectController> objectController)
{
    if (auto element = objectController->GetCatalogReference(); element)
    {
        return Builder::transform<CatalogReference>(element);
    }
    if (auto element = objectController->GetController(); element)
    {
        return Builder::transform<Controller>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IObjectController");
}

ObjectControllerBase::ObjectControllerBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IObjectController> objectController)
    : Node{"ObjectControllerBase"}, objectController_{(std::cout << "ObjectControllerBase instantiating objectController_" << std::endl, resolve_choices(objectController))}

{
}

bool ObjectControllerBase::Complete() const
{
    std::cout << "ObjectControllerBase complete?\n";
    return OPENSCENARIO::Complete(objectController_);
}

void ObjectControllerBase::Step()
{
    std::cout << "ObjectControllerBase step!\n";
    OPENSCENARIO::Step(objectController_);
}

void ObjectControllerBase::Stop()
{
    std::cout << "ObjectControllerBase stop!\n";
    OPENSCENARIO::Stop(objectController_);
}

}  // namespace OPENSCENARIO
