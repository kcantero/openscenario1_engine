/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <OpenScenarioParser/openScenarioLib/v1_0/generated/api/ApiClassInterfaces.h>

#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "Common/Node.h"

namespace OPENSCENARIO
{
class InitActionsBase : public Node
{
  public:
    using GlobalActions_t = std::vector<std::unique_ptr<Node>>;
    using UserDefinedActions_t = std::vector<std::unique_ptr<Node>>;
    using Privates_t = std::vector<std::unique_ptr<Node>>;

    explicit InitActionsBase(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IInitActions> initActions);
    InitActionsBase(InitActionsBase&&) = default;
    ~InitActionsBase() override = default;

    bool Complete() const override;
    void Step() override;
    void Stop() override;
  protected:
    GlobalActions_t globalActions_;
    UserDefinedActions_t userDefinedActions_;
    Privates_t privates_;
};

}  // namespace OPENSCENARIO
