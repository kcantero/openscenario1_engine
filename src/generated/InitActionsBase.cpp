/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "InitActionsBase.h"

#include "GlobalAction.h"
#include "OpenScenarioEngineFactory.h"
#include "Private.h"
#include "UserDefinedAction.h"

namespace OPENSCENARIO
{
InitActionsBase::InitActionsBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IInitActions> initActions)
    : Node{"InitActionsBase"}, globalActions_{(std::cout << "InitActionsBase instantiating globalActions_" << std::endl, Builder::transform<GlobalAction, GlobalActions_t>(initActions, &NET_ASAM_OPENSCENARIO::v1_0::IInitActions::GetGlobalActions))}, userDefinedActions_{(std::cout << "InitActionsBase instantiating userDefinedActions_" << std::endl, Builder::transform<UserDefinedAction, UserDefinedActions_t>(initActions, &NET_ASAM_OPENSCENARIO::v1_0::IInitActions::GetUserDefinedActions))}, privates_{(std::cout << "InitActionsBase instantiating privates_" << std::endl, Builder::transform<Private, Privates_t>(initActions, &NET_ASAM_OPENSCENARIO::v1_0::IInitActions::GetPrivates))}

{
}

bool InitActionsBase::Complete() const
{
    std::cout << "InitActionsBase complete?\n";
    return OPENSCENARIO::Complete(globalActions_) && OPENSCENARIO::Complete(userDefinedActions_) && OPENSCENARIO::Complete(privates_);
}

void InitActionsBase::Step()
{
    std::cout << "InitActionsBase step!\n";
    OPENSCENARIO::Step(globalActions_);
    OPENSCENARIO::Step(userDefinedActions_);
    OPENSCENARIO::Step(privates_);
}

void InitActionsBase::Stop()
{
    std::cout << "InitActionsBase stop!\n";
    OPENSCENARIO::Stop(globalActions_);
    OPENSCENARIO::Stop(userDefinedActions_);
    OPENSCENARIO::Stop(privates_);
}

}  // namespace OPENSCENARIO
