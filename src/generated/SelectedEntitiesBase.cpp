/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SelectedEntitiesBase.h"

#include "OpenScenarioEngineFactory.h"

#include "EntityRef.h"
#include "ByType.h"

namespace OPENSCENARIO
{
static SelectedEntitiesBase::SelectedEntities_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ISelectedEntities> selectedEntities)
{
    if (auto elements = selectedEntities->GetEntityRef(); !elements.empty())
    {
        return Builder::transform<EntityRef>(elements);
    }
    if (auto elements = selectedEntities->GetByType(); !elements.empty())
    {
        return Builder::transform<ByType>(elements);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ISelectedEntities");
}

SelectedEntitiesBase::SelectedEntitiesBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ISelectedEntities> selectedEntities)
    : Node{"SelectedEntitiesBase"}, selectedEntities_{(std::cout << "SelectedEntitiesBase instantiating selectedEntities_" << std::endl, resolve_choices(selectedEntities))}

{
}

bool SelectedEntitiesBase::Complete() const
{
    std::cout << "SelectedEntitiesBase complete?\n";
    return OPENSCENARIO::Complete(selectedEntities_);
}

void SelectedEntitiesBase::Step()
{
    std::cout << "SelectedEntitiesBase step!\n";
    OPENSCENARIO::Step(selectedEntities_);
}

void SelectedEntitiesBase::Stop()
{
    std::cout << "SelectedEntitiesBase stop!\n";
    OPENSCENARIO::Stop(selectedEntities_);
}

}  // namespace OPENSCENARIO
