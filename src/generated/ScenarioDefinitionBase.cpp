/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ScenarioDefinitionBase.h"

#include "CatalogLocations.h"
#include "Entities.h"
#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"
#include "RoadNetwork.h"
#include "Storyboard.h"

namespace OPENSCENARIO
{
ScenarioDefinitionBase::ScenarioDefinitionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IScenarioDefinition> scenarioDefinition)
    : Node{"ScenarioDefinitionBase"}, catalogLocations_{(std::cout << "ScenarioDefinitionBase instantiating catalogLocations_" << std::endl, Builder::transform<CatalogLocations>(scenarioDefinition->GetCatalogLocations()))}, roadNetwork_{(std::cout << "ScenarioDefinitionBase instantiating roadNetwork_" << std::endl, Builder::transform<RoadNetwork>(scenarioDefinition->GetRoadNetwork()))}, entities_{(std::cout << "ScenarioDefinitionBase instantiating entities_" << std::endl, Builder::transform<Entities>(scenarioDefinition->GetEntities()))}, storyboard_{(std::cout << "ScenarioDefinitionBase instantiating storyboard_" << std::endl, Builder::transform<Storyboard>(scenarioDefinition->GetStoryboard()))}, parameterDeclarations_{(std::cout << "ScenarioDefinitionBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(scenarioDefinition, &NET_ASAM_OPENSCENARIO::v1_0::IScenarioDefinition::GetParameterDeclarations))}

{
}

bool ScenarioDefinitionBase::Complete() const
{
    std::cout << "ScenarioDefinitionBase complete?\n";
    return OPENSCENARIO::Complete(catalogLocations_) && OPENSCENARIO::Complete(roadNetwork_) && OPENSCENARIO::Complete(entities_) && OPENSCENARIO::Complete(storyboard_) && OPENSCENARIO::Complete(parameterDeclarations_);
}

void ScenarioDefinitionBase::Step()
{
    std::cout << "ScenarioDefinitionBase step!\n";
    OPENSCENARIO::Step(catalogLocations_);
    OPENSCENARIO::Step(roadNetwork_);
    OPENSCENARIO::Step(entities_);
    OPENSCENARIO::Step(storyboard_);
    OPENSCENARIO::Step(parameterDeclarations_);
}

void ScenarioDefinitionBase::Stop()
{
    std::cout << "ScenarioDefinitionBase stop!\n";
    OPENSCENARIO::Stop(catalogLocations_);
    OPENSCENARIO::Stop(roadNetwork_);
    OPENSCENARIO::Stop(entities_);
    OPENSCENARIO::Stop(storyboard_);
    OPENSCENARIO::Stop(parameterDeclarations_);
}

}  // namespace OPENSCENARIO
