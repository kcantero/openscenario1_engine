/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RelativeWorldPositionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Orientation.h"

namespace OPENSCENARIO
{
RelativeWorldPositionBase::RelativeWorldPositionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRelativeWorldPosition> relativeWorldPosition)
    : Node{"RelativeWorldPositionBase"}, orientation_{(std::cout << "RelativeWorldPositionBase instantiating orientation_" << std::endl, Builder::transform<Orientation>(relativeWorldPosition->GetOrientation()))}

{
}

bool RelativeWorldPositionBase::Complete() const
{
    std::cout << "RelativeWorldPositionBase complete?\n";
    return OPENSCENARIO::Complete(orientation_);
}

void RelativeWorldPositionBase::Step()
{
    std::cout << "RelativeWorldPositionBase step!\n";
    OPENSCENARIO::Step(orientation_);
}

void RelativeWorldPositionBase::Stop()
{
    std::cout << "RelativeWorldPositionBase stop!\n";
    OPENSCENARIO::Stop(orientation_);
}

}  // namespace OPENSCENARIO
