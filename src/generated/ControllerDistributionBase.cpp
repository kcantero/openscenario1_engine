/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ControllerDistributionBase.h"

#include "ControllerDistributionEntry.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
ControllerDistributionBase::ControllerDistributionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IControllerDistribution> controllerDistribution)
    : Node{"ControllerDistributionBase"}, controllerDistributionEntries_{(std::cout << "ControllerDistributionBase instantiating controllerDistributionEntries_" << std::endl, Builder::transform<ControllerDistributionEntry, ControllerDistributionEntries_t>(controllerDistribution, &NET_ASAM_OPENSCENARIO::v1_0::IControllerDistribution::GetControllerDistributionEntries))}

{
}

bool ControllerDistributionBase::Complete() const
{
    std::cout << "ControllerDistributionBase complete?\n";
    return OPENSCENARIO::Complete(controllerDistributionEntries_);
}

void ControllerDistributionBase::Step()
{
    std::cout << "ControllerDistributionBase step!\n";
    OPENSCENARIO::Step(controllerDistributionEntries_);
}

void ControllerDistributionBase::Stop()
{
    std::cout << "ControllerDistributionBase stop!\n";
    OPENSCENARIO::Stop(controllerDistributionEntries_);
}

}  // namespace OPENSCENARIO
