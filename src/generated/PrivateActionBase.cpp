/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PrivateActionBase.h"

#include "ActivateControllerAction.h"
#include "ControllerAction.h"
#include "LateralAction.h"
#include "LongitudinalAction.h"
#include "OpenScenarioEngineFactory.h"
#include "RoutingAction.h"
#include "SynchronizeAction.h"
#include "TeleportAction.h"
#include "VisibilityAction.h"

namespace OPENSCENARIO
{
static PrivateActionBase::PrivateAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPrivateAction> privateAction)
{
    if (auto element = privateAction->GetLongitudinalAction(); element)
    {
        return Builder::transform<LongitudinalAction>(element);
    }
    if (auto element = privateAction->GetLateralAction(); element)
    {
        return Builder::transform<LateralAction>(element);
    }
    if (auto element = privateAction->GetVisibilityAction(); element)
    {
        return Builder::transform<VisibilityAction>(element);
    }
    if (auto element = privateAction->GetSynchronizeAction(); element)
    {
        return Builder::transform<SynchronizeAction>(element);
    }
    if (auto element = privateAction->GetActivateControllerAction(); element)
    {
        return Builder::transform<ActivateControllerAction>(element);
    }
    if (auto element = privateAction->GetControllerAction(); element)
    {
        return Builder::transform<ControllerAction>(element);
    }
    if (auto element = privateAction->GetTeleportAction(); element)
    {
        return Builder::transform<TeleportAction>(element);
    }
    if (auto element = privateAction->GetRoutingAction(); element)
    {
        return Builder::transform<RoutingAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IPrivateAction");
}

PrivateActionBase::PrivateActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPrivateAction> privateAction)
    : Node{"PrivateActionBase"}, privateAction_{(std::cout << "PrivateActionBase instantiating privateAction_" << std::endl, resolve_choices(privateAction))}

{
}

bool PrivateActionBase::Complete() const
{
    std::cout << "PrivateActionBase complete?\n";
    return OPENSCENARIO::Complete(privateAction_);
}

void PrivateActionBase::Step()
{
    std::cout << "PrivateActionBase step!\n";
    OPENSCENARIO::Step(privateAction_);
}

void PrivateActionBase::Stop()
{
    std::cout << "PrivateActionBase stop!\n";
    OPENSCENARIO::Stop(privateAction_);
}

}  // namespace OPENSCENARIO
