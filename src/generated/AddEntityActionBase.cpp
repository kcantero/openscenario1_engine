/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AddEntityActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
AddEntityActionBase::AddEntityActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAddEntityAction> addEntityAction)
    : Node{"AddEntityActionBase"}, position_{(std::cout << "AddEntityActionBase instantiating position_" << std::endl, Builder::transform<Position>(addEntityAction->GetPosition()))}

{
}

bool AddEntityActionBase::Complete() const
{
    std::cout << "AddEntityActionBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void AddEntityActionBase::Step()
{
    std::cout << "AddEntityActionBase step!\n";
    OPENSCENARIO::Step(position_);
}

void AddEntityActionBase::Stop()
{
    std::cout << "AddEntityActionBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO
