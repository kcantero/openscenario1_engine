/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ConditionGroupBase.h"

#include "Condition.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
ConditionGroupBase::ConditionGroupBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IConditionGroup> conditionGroup,
    mantle_api::IEnvironment& environment)
    : Node{"ConditionGroupBase"}, conditions_{(std::cout << "ConditionGroupBase instantiating conditions_" << std::endl, Builder::transform<Condition, Conditions_t>(conditionGroup, &NET_ASAM_OPENSCENARIO::v1_0::IConditionGroup::GetConditions))}

      ,
      environment_{environment}
{
}

bool ConditionGroupBase::Complete() const
{
    std::cout << "ConditionGroupBase complete?\n";
    return OPENSCENARIO::Complete(conditions_);
}

void ConditionGroupBase::Step()
{
    std::cout << "ConditionGroupBase step!\n";
    OPENSCENARIO::Step(conditions_);
}

void ConditionGroupBase::Stop()
{
    std::cout << "ConditionGroupBase stop!\n";
    OPENSCENARIO::Stop(conditions_);
}

}  // namespace OPENSCENARIO
