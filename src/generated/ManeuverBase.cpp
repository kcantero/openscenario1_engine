/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ManeuverBase.h"

#include "Event.h"
#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"

namespace OPENSCENARIO
{
ManeuverBase::ManeuverBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IManeuver> maneuver)
    : Node{"ManeuverBase"}, parameterDeclarations_{(std::cout << "ManeuverBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(maneuver, &NET_ASAM_OPENSCENARIO::v1_0::IManeuver::GetParameterDeclarations))}, events_{(std::cout << "ManeuverBase instantiating events_" << std::endl, Builder::transform<Event, Events_t>(maneuver, &NET_ASAM_OPENSCENARIO::v1_0::IManeuver::GetEvents))}

{
}

bool ManeuverBase::Complete() const
{
    std::cout << "ManeuverBase complete?\n";
    return OPENSCENARIO::Complete(parameterDeclarations_) && OPENSCENARIO::Complete(events_);
}

void ManeuverBase::Step()
{
    std::cout << "ManeuverBase step!\n";
    OPENSCENARIO::Step(parameterDeclarations_);
    OPENSCENARIO::Step(events_);
}

void ManeuverBase::Stop()
{
    std::cout << "ManeuverBase stop!\n";
    OPENSCENARIO::Stop(parameterDeclarations_);
    OPENSCENARIO::Stop(events_);
}

}  // namespace OPENSCENARIO
