/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LaneChangeActionBase.h"

#include "LaneChangeTarget.h"
#include "OpenScenarioEngineFactory.h"
#include "TransitionDynamics.h"

namespace OPENSCENARIO
{
LaneChangeActionBase::LaneChangeActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILaneChangeAction> laneChangeAction)
    : Node{"LaneChangeActionBase"}, laneChangeActionDynamics_{(std::cout << "LaneChangeActionBase instantiating laneChangeActionDynamics_" << std::endl, Builder::transform<TransitionDynamics>(laneChangeAction->GetLaneChangeActionDynamics()))}, laneChangeTarget_{(std::cout << "LaneChangeActionBase instantiating laneChangeTarget_" << std::endl, Builder::transform<LaneChangeTarget>(laneChangeAction->GetLaneChangeTarget()))}

{
}

bool LaneChangeActionBase::Complete() const
{
    std::cout << "LaneChangeActionBase complete?\n";
    return OPENSCENARIO::Complete(laneChangeActionDynamics_) && OPENSCENARIO::Complete(laneChangeTarget_);
}

void LaneChangeActionBase::Step()
{
    std::cout << "LaneChangeActionBase step!\n";
    OPENSCENARIO::Step(laneChangeActionDynamics_);
    OPENSCENARIO::Step(laneChangeTarget_);
}

void LaneChangeActionBase::Stop()
{
    std::cout << "LaneChangeActionBase stop!\n";
    OPENSCENARIO::Stop(laneChangeActionDynamics_);
    OPENSCENARIO::Stop(laneChangeTarget_);
}

}  // namespace OPENSCENARIO
