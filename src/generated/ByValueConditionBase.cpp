/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ByValueConditionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "ParameterCondition.h"
#include "SimulationTimeCondition.h"
#include "StoryboardElementStateCondition.h"
#include "TimeOfDayCondition.h"
#include "TrafficSignalCondition.h"
#include "TrafficSignalControllerCondition.h"
#include "UserDefinedValueCondition.h"

namespace OPENSCENARIO
{
static ByValueConditionBase::ByValueCondition_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IByValueCondition> byValueCondition)
{
    if (auto element = byValueCondition->GetParameterCondition(); element)
    {
        return Builder::transform<ParameterCondition>(element);
    }
    if (auto element = byValueCondition->GetTimeOfDayCondition(); element)
    {
        return Builder::transform<TimeOfDayCondition>(element);
    }
    if (auto element = byValueCondition->GetSimulationTimeCondition(); element)
    {
        return Builder::transform<SimulationTimeCondition>(element);
    }
    if (auto element = byValueCondition->GetStoryboardElementStateCondition(); element)
    {
        return Builder::transform<StoryboardElementStateCondition>(element);
    }
    if (auto element = byValueCondition->GetUserDefinedValueCondition(); element)
    {
        return Builder::transform<UserDefinedValueCondition>(element);
    }
    if (auto element = byValueCondition->GetTrafficSignalCondition(); element)
    {
        return Builder::transform<TrafficSignalCondition>(element);
    }
    if (auto element = byValueCondition->GetTrafficSignalControllerCondition(); element)
    {
        return Builder::transform<TrafficSignalControllerCondition>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IByValueCondition");
}

ByValueConditionBase::ByValueConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IByValueCondition> byValueCondition,
    mantle_api::IEnvironment& environment)
    : Node{"ByValueConditionBase"}, byValueCondition_{(std::cout << "ByValueConditionBase instantiating byValueCondition_" << std::endl, resolve_choices(byValueCondition))}

      ,
      environment_{environment}
{
}

bool ByValueConditionBase::Complete() const
{
    std::cout << "ByValueConditionBase complete?\n";
    return OPENSCENARIO::Complete(byValueCondition_);
}

void ByValueConditionBase::Step()
{
    std::cout << "ByValueConditionBase step!\n";
    OPENSCENARIO::Step(byValueCondition_);
}

void ByValueConditionBase::Stop()
{
    std::cout << "ByValueConditionBase stop!\n";
    OPENSCENARIO::Stop(byValueCondition_);
}

}  // namespace OPENSCENARIO
