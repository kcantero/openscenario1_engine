/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ControllerActionBase.h"

#include "AssignControllerAction.h"
#include "OpenScenarioEngineFactory.h"
#include "OverrideControllerValueAction.h"

namespace OPENSCENARIO
{
ControllerActionBase::ControllerActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IControllerAction> controllerAction)
    : Node{"ControllerActionBase"}, assignControllerAction_{(std::cout << "ControllerActionBase instantiating assignControllerAction_" << std::endl, Builder::transform<AssignControllerAction>(controllerAction->GetAssignControllerAction()))}, overrideControllerValueAction_{(std::cout << "ControllerActionBase instantiating overrideControllerValueAction_" << std::endl, Builder::transform<OverrideControllerValueAction>(controllerAction->GetOverrideControllerValueAction()))}

{
}

bool ControllerActionBase::Complete() const
{
    std::cout << "ControllerActionBase complete?\n";
    return OPENSCENARIO::Complete(assignControllerAction_) && OPENSCENARIO::Complete(overrideControllerValueAction_);
}

void ControllerActionBase::Step()
{
    std::cout << "ControllerActionBase step!\n";
    OPENSCENARIO::Step(assignControllerAction_);
    OPENSCENARIO::Step(overrideControllerValueAction_);
}

void ControllerActionBase::Stop()
{
    std::cout << "ControllerActionBase stop!\n";
    OPENSCENARIO::Stop(assignControllerAction_);
    OPENSCENARIO::Stop(overrideControllerValueAction_);
}

}  // namespace OPENSCENARIO
