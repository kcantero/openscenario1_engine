/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <OpenScenarioParser/openScenarioLib/v1_0/generated/api/ApiClassInterfaces.h>

#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "Common/Node.h"

namespace OPENSCENARIO
{
class TrafficSinkActionBase : public Node
{
  public:
    explicit TrafficSinkActionBase(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficSinkAction> trafficSinkAction);
    TrafficSinkActionBase(TrafficSinkActionBase&&) = default;
    ~TrafficSinkActionBase() override = default;

    bool Complete() const override;
    void Step() override;
    void Stop() override;
  protected:
    std::unique_ptr<Node> position_;
    std::unique_ptr<Node> trafficDefinition_;
};

}  // namespace OPENSCENARIO
