/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "DistanceConditionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
DistanceConditionBase::DistanceConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IDistanceCondition> distanceCondition,
    mantle_api::IEnvironment& environment)
    : Node{"DistanceConditionBase"}, position_{(std::cout << "DistanceConditionBase instantiating position_" << std::endl, Builder::transform<Position>(distanceCondition->GetPosition()))}, rule_{Rule::from(distanceCondition)}, environment_{environment}
{
}

bool DistanceConditionBase::Complete() const
{
    std::cout << "DistanceConditionBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void DistanceConditionBase::Step()
{
    std::cout << "DistanceConditionBase step!\n";
    OPENSCENARIO::Step(position_);
}

void DistanceConditionBase::Stop()
{
    std::cout << "DistanceConditionBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO
