/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LaneOffsetTargetBase.h"

#include "AbsoluteTargetLaneOffset.h"
#include "OpenScenarioEngineFactory.h"
#include "RelativeTargetLaneOffset.h"

namespace OPENSCENARIO
{
static LaneOffsetTargetBase::LaneOffsetTarget_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILaneOffsetTarget> laneOffsetTarget)
{
    if (auto element = laneOffsetTarget->GetRelativeTargetLaneOffset(); element)
    {
        return Builder::transform<RelativeTargetLaneOffset>(element);
    }
    if (auto element = laneOffsetTarget->GetAbsoluteTargetLaneOffset(); element)
    {
        return Builder::transform<AbsoluteTargetLaneOffset>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ILaneOffsetTarget");
}

LaneOffsetTargetBase::LaneOffsetTargetBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILaneOffsetTarget> laneOffsetTarget)
    : Node{"LaneOffsetTargetBase"}, laneOffsetTarget_{(std::cout << "LaneOffsetTargetBase instantiating laneOffsetTarget_" << std::endl, resolve_choices(laneOffsetTarget))}

{
}

bool LaneOffsetTargetBase::Complete() const
{
    std::cout << "LaneOffsetTargetBase complete?\n";
    return OPENSCENARIO::Complete(laneOffsetTarget_);
}

void LaneOffsetTargetBase::Step()
{
    std::cout << "LaneOffsetTargetBase step!\n";
    OPENSCENARIO::Step(laneOffsetTarget_);
}

void LaneOffsetTargetBase::Stop()
{
    std::cout << "LaneOffsetTargetBase stop!\n";
    OPENSCENARIO::Stop(laneOffsetTarget_);
}

}  // namespace OPENSCENARIO
