/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OpenScenarioCategoryBase.h"

#include "CatalogDefinition.h"
#include "OpenScenarioEngineFactory.h"
#include "ScenarioDefinition.h"

namespace OPENSCENARIO
{
static OpenScenarioCategoryBase::OpenScenarioCategory_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IOpenScenarioCategory> openScenarioCategory)
{
    if (auto element = openScenarioCategory->GetScenarioDefinition(); element)
    {
        return Builder::transform<ScenarioDefinition>(element);
    }
    if (auto element = openScenarioCategory->GetCatalogDefinition(); element)
    {
        return Builder::transform<CatalogDefinition>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IOpenScenarioCategory");
}

OpenScenarioCategoryBase::OpenScenarioCategoryBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IOpenScenarioCategory> openScenarioCategory)
    : Node{"OpenScenarioCategoryBase"}, openScenarioCategory_{(std::cout << "OpenScenarioCategoryBase instantiating openScenarioCategory_" << std::endl, resolve_choices(openScenarioCategory))}

{
}

bool OpenScenarioCategoryBase::Complete() const
{
    std::cout << "OpenScenarioCategoryBase complete?\n";
    return OPENSCENARIO::Complete(openScenarioCategory_);
}

void OpenScenarioCategoryBase::Step()
{
    std::cout << "OpenScenarioCategoryBase step!\n";
    OPENSCENARIO::Step(openScenarioCategory_);
}

void OpenScenarioCategoryBase::Stop()
{
    std::cout << "OpenScenarioCategoryBase stop!\n";
    OPENSCENARIO::Stop(openScenarioCategory_);
}

}  // namespace OPENSCENARIO
