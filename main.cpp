/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <iostream>
#include <memory>
#include <string>

#include <OpenScenarioParser/openScenarioLib/common/loader/FileResourceLocator.h>
#include <OpenScenarioParser/openScenarioLib/v1_0/src/loader/XmlScenarioImportLoaderFactory.h>

#include "OpenScenarioEngineFactory.h"
#include "OpenScenarioEngineTypes.h"
#include "SimpleMessageLogger.h"
#include "Storyboard.h"
#include "fakeEnvironment.h"

using namespace units::literals;

auto ParseScenarioFile(const std::string scenario_file_path)
{
    auto message_logger =
        std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
    auto catalog_message_logger =
        std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
    auto loader_factory =
        NET_ASAM_OPENSCENARIO::v1_0::XmlScenarioImportLoaderFactory(catalog_message_logger, scenario_file_path);
    auto loader = loader_factory.CreateLoader(std::make_shared<NET_ASAM_OPENSCENARIO::FileResourceLocator>());
    auto scenario_data_ptr_ = std::static_pointer_cast<NET_ASAM_OPENSCENARIO::v1_0::IOpenScenario>(
        loader->Load(message_logger)->GetAdapter(typeid(NET_ASAM_OPENSCENARIO::v1_0::IOpenScenario).name()));
    return scenario_data_ptr_;
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cout << "Usage: ./openScenarioEngine <path>/scenario.xosc\n";
        return 0;
    }

    // create environment and register to factory
    std::shared_ptr<mantle_api::IEnvironment> env = std::make_shared<FakeEnvironment>();
    OPENSCENARIO::Factory::Dependencies.SetEnvironment(env);

    // get scenario
    std::string scenario_file{argv[1]};
    std::cout << "parsing " << scenario_file << '\n';
    auto open_scenario_ptr = ParseScenarioFile(scenario_file);

    if (!(open_scenario_ptr && open_scenario_ptr->GetOpenScenarioCategory() &&
          open_scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition()))
    {
        throw std::runtime_error("Scenario file not found or file does not contain scenario definition.");
    }

    auto scenario_definition_ptr_ = open_scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition();
    std::cout << "starting...\n";

    if (auto i_storyboard = scenario_definition_ptr_->GetStoryboard(); i_storyboard)
    {
        OPENSCENARIO::Storyboard storyboard(i_storyboard);

        for (int i = 0; i < 10; ++i)  // just in case something goes wrong
        {
            std::cout << "----------------------\n";
            auto current_time = env->GetDateTime().date_time;
            std::cout << "Current Time: " << units::time::to_string(current_time) << '\n';

            storyboard.Step();

            if (storyboard.Complete())
            {
                std::cout << "done\n";
                return 0;
            }

            env->SetDateTime(mantle_api::DateTime{current_time + 1_s});
        }
        std::cout << "... did not finish.\n";
        return -1;
    }
    else
    {
        throw std::runtime_error("Scenario does not contain storyboard.");
    }
}
